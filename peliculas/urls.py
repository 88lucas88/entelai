from django.conf.urls import url

from .views import *

app_name = 'peliculas'

urlpatterns = [
    url(r'upload-csv/', csv_upload, name="csv_upload"),
    url(r'costo-total/', costo_total, name="costo_total"),
    url(r'costo-promedio/', costo_promedio, name="costo_promedio"),

]