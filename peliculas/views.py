from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import pandas as pd
import re
from .models import Pelicula
# Create your views here.

def csv_upload(request):
    
    Pelicula.objects.all().delete() #Solo para este ejercicio, evitar errores de integridad

    csv_file = request.FILES['csv_file']

    df=pd.read_csv(csv_file,sep=',')
    df['year'] = df['year'].replace(to_replace=r'[A-Za-z]+.', value='', regex=True)
    df['date_published'] = df['date_published'].replace(to_replace=r'[A-Za-z]+.', value='', regex=True)
    df['date_published'] = pd.to_datetime(df['date_published'],errors='ignore', format="%Y/%d/%m",infer_datetime_format=True)
    df = df.fillna(0)
    
    peliculas = []
    for e in df.T.to_dict().values():
        peliculas.append(Pelicula(**e))

    Pelicula.objects.bulk_create(peliculas)
    
    return HttpResponseRedirect('/')

#http://127.0.0.1:8000/peliculas/costo-total/?pais=USA (o cualquier pais)
def costo_total(request):
    """costo total (budget) de todas las películas cuyo país es USA"""
    pais = request.GET.get('pais')
    peliculas = Pelicula.objects.all().filter(country=pais)
    costo_total = 0
    for p in peliculas: 
        costo_total += float(re.sub('[A-Z]|[\$].','', p.budget))
    
    return JsonResponse({"pais":pais,"costo-total": costo_total})

#http://localhost:8000/peliculas/costo-promedio/?pais=USA
def costo_promedio(request):
    """costo promedio por película de todas las películas cuyo país es USA"""
    pais = request.GET.get('pais')
    peliculas = Pelicula.objects.all().filter(country=pais).filter(budget__lt=0)  
    costo_total = 0
    costo_primedio = 0
    for p in peliculas: 
        costo_total += float(re.sub('[A-Z]|[\$].','', p.budget))
    costo_primedio = round(costo_total/len(peliculas),2)
    return JsonResponse({"pais":pais,"costo_primedio": costo_primedio})
    